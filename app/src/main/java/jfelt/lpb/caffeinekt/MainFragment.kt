package jfelt.lpb.caffeinekt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import jfelt.lpb.caffeinekt.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var nullBinding: FragmentMainBinding? = null
    private val binding get() = nullBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        nullBinding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val a = requireActivity() as MainActivity
        a.supportActionBar?.title = "Home"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        nullBinding = null
    }
}