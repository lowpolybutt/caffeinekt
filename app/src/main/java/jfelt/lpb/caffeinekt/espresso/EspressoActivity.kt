package jfelt.lpb.caffeinekt.espresso

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import jfelt.lpb.caffeinekt.MainActivity
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.Utils
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanViewModel
import jfelt.lpb.caffeinekt.bean.NewBeanDialog
import jfelt.lpb.caffeinekt.databinding.ActivityEspressoMainBinding
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachine
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachineViewModel
import jfelt.lpb.caffeinekt.espressomachines.NewEspressoMachineDialog
import jfelt.lpb.caffeinekt.grinder.Grinder
import jfelt.lpb.caffeinekt.grinder.GrinderViewModel
import jfelt.lpb.caffeinekt.grinder.NewGrinderDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class EspressoActivity : AppCompatActivity(), NewBeanDialog.OnBeanAddedListener, NewEspressoMachineDialog.OnEspressoMachineAddedListener,
                            NewGrinderDialog.OnGrinderAddedListener {

    private lateinit var binding: ActivityEspressoMainBinding

    private val beanVm: BeanViewModel by lazy { ViewModelProvider(this)[BeanViewModel::class.java] }
    private val machineVm: EspressoMachineViewModel by lazy { ViewModelProvider(this)[EspressoMachineViewModel::class.java] }
    private val grinderVm: GrinderViewModel by lazy { ViewModelProvider(this)[GrinderViewModel::class.java] }

    private lateinit var selectedBean: Bean
    private lateinit var selectedMachine: EspressoMachine
    private lateinit var selectedGrinder: Grinder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEspressoMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val tb = binding.activityEspressoToolbar

        setSupportActionBar(tb)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }

        initBeanForm()
        initMachineForm()
        initGrinderForm()

        binding.activityEspressoBrewContinue.setOnClickListener { prepAndSubmitModel() }
    }

    override fun onBeanAdded(b: Bean) {
        beanVm.addNewBean(b).invokeOnCompletion {
            MainScope().launch(Dispatchers.Main) { binding.activityEspressoNewBeanChecker.aeropressBrewNewBean.isChecked = true }
        }
    }

    override fun onMachineAdded(m: EspressoMachine) {
        machineVm.insertMachine(m).invokeOnCompletion {
            MainScope().launch(Dispatchers.Main) { binding.activityEspressoNewMachineChecker.espressoNewMachineCheckbox.isChecked = true }
        }
    }

    override fun onGrinderAdded(g: Grinder) {
        grinderVm.insertGrinder(g).invokeOnCompletion {
            MainScope().launch(Dispatchers.Main) { binding.activityEspressoGrinderChecker.existingGrinderCheckbox.isChecked = true }
        }
    }

    private fun prepAndSubmitModel() {
        val preinfusion = binding.activityEspressoBrewMethodPreinfusion.editText!!
        val coffeeMass = binding.activityEspressoBrewMethodCoffeeMass.editText!!
        val waterMass = binding.activityEspressoBrewMethodBrewMassTarget.editText!!
        val time = binding.activityEspressoBrewMethodTimeTarget.editText!!

        var preinfusionValue: Int? = null
        var coffeeMassValue: Float? = null
        var waterMassValue: Float? = null
        var timeValue: Int? = null

        if (isInputFilled(preinfusion)) {
            preinfusionValue = preinfusion.text.toString().toInt()
        } else {
            preinfusion.error = "Field is required!"
        }

        if (isInputFilled(coffeeMass)) {
            coffeeMassValue = coffeeMass.text.toString().toFloat()
        } else {
            coffeeMass.error = "Field is required!"
        }

        if (isInputFilled(waterMass)) {
            waterMassValue = waterMass.text.toString().toFloat()
        } else {
            waterMass.error = "Field is required!"
        }

        if (isInputFilled(time)) {
            timeValue = time.text.toString().toInt()
        } else {
            time.error = "Field is required!"
        }

        val brew = Espresso(null, selectedBean, selectedMachine, selectedGrinder,
                            preinfusionValue, coffeeMassValue!!, waterMassValue!!, timeValue, null, null)

        val dao = EspressoDatabase.getInstance(this).espressoDao()
        val repo = EspressoRepo(dao)
        MainScope().launch(Dispatchers.IO) { repo.insert(brew) }.invokeOnCompletion {
            Utils.debugLog(msg = "Inserted $brew")
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun isInputFilled(input: EditText) = input.text.toString() != ""

    private fun initBeanForm() {
        val form = binding.activityEspressoNewBeanChecker
        val checker = form.aeropressBrewNewBean
        val autoComplete = form.aeropressChooseBeanLayout
        val autoCompleteInput = autoComplete.editText as? AutoCompleteTextView
        val addBean = form.aeropressBrewAddNewBeanButton

        addBean.setOnClickListener {
            val b = NewBeanDialog(this, null)
            b.show(supportFragmentManager, b.tag)
        }

        beanVm.allBeans.observe(this, Observer {
            val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, it)
            autoCompleteInput?.apply {
                setOnItemClickListener { _, _, pos, _ ->
                    selectedBean = it[pos]
                }
                setAdapter(adapter)
            }
        })


        checker.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked) {
                true -> {
                    autoComplete.visibility = View.VISIBLE
                    addBean.visibility = View.GONE
                }
                false -> {
                    autoComplete.visibility = View.GONE
                    addBean.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun initMachineForm() {
        val form = binding.activityEspressoNewMachineChecker
        val checkbox = form.espressoNewMachineCheckbox
        val newMachineBtn = form.espressoMachineNewMachineButton
        val autoCompleteLayout = form.espressoMachinePicker
        val autoComplete = autoCompleteLayout.editText as? AutoCompleteTextView

        newMachineBtn.setOnClickListener {
            val mD = NewEspressoMachineDialog(this)
            mD.show(supportFragmentManager, mD.tag)
        }

        machineVm.allMachines.observe(this, Observer {
            val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, it)
            autoComplete?.apply {
                setOnItemClickListener { _, _, position, _ ->
                    selectedMachine = it[position]
                }
                setAdapter(adapter)
            }
        })

        checkbox.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked) {
                true -> {
                    newMachineBtn.visibility = View.GONE
                    autoCompleteLayout.visibility = View.VISIBLE
                }
                false -> {
                    newMachineBtn.visibility = View.VISIBLE
                    autoCompleteLayout.visibility = View.GONE
                }
            }
        }
    }

    private fun initGrinderForm() {
        val form = binding.activityEspressoGrinderChecker
        val checkbox = form.existingGrinderCheckbox
        val newGrinder = form.existingGrinderNewGrinder
        val autoCompleteLayout = form.existingGrinderSelect
        val autoComplete = form.existingGrinderSelect.editText as? AutoCompleteTextView

        checkbox.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked) {
                true -> {
                    newGrinder.visibility = View.GONE
                    autoCompleteLayout.visibility = View.VISIBLE
                }
                false -> {
                    newGrinder.visibility = View.VISIBLE
                    autoCompleteLayout.visibility = View.GONE
                }
            }
        }

        newGrinder.setOnClickListener {
            val grinderDialog = NewGrinderDialog(this)
            grinderDialog.show(supportFragmentManager, grinderDialog.tag)
        }

        grinderVm.allGrinders.observe(this, Observer {
            val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, it)
            autoComplete?.apply {
                setAdapter(adapter)
                setOnItemClickListener { _, _, position, _ ->
                    selectedGrinder = it[position]
                }
            }
        })
    }
}