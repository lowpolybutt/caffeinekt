package jfelt.lpb.caffeinekt.espresso

class EspressoRepo(private val dao: EspressoDao) {
    val allBrews = dao.getAllEspressoBrews()

    suspend fun insert(e: Espresso) = dao.insert(e)
    suspend fun delete(e: Espresso) = dao.delete(e)
}