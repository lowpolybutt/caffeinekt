package jfelt.lpb.caffeinekt.espresso

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EspressoBrewViewModel(app: Application) : AndroidViewModel(app) {
    val repo: EspressoRepo

    val allBrews: LiveData<List<Espresso>>

    init {
        val dao = EspressoDatabase.getInstance(app).espressoDao()
        repo = EspressoRepo(dao)

        allBrews = dao.getAllEspressoBrews()
    }

    fun insertBrew(e: Espresso) = viewModelScope.launch(Dispatchers.IO) { repo.insert(e) }
    fun deleteBrew(e: Espresso) = viewModelScope.launch(Dispatchers.IO) { repo.delete(e) }
}