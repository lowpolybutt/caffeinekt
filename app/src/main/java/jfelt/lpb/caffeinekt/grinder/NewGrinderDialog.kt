package jfelt.lpb.caffeinekt.grinder

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import jfelt.lpb.caffeinekt.Utils
import jfelt.lpb.caffeinekt.databinding.DialogNewGrinderBinding

class NewGrinderDialog(private val li: OnGrinderAddedListener) : DialogFragment() {

    private var _binding: DialogNewGrinderBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = DialogNewGrinderBinding.inflate(LayoutInflater.from(requireContext()))

        val manufacturerInput = binding.dialogGrinderManufacturer.editText!!
        val modelInput = binding.dialogGrinderModel.editText!!

        val inputs = arrayListOf(manufacturerInput, modelInput)
        inputs.all {  et ->
            et.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {}

                override fun onTextChanged(
                    s: CharSequence?,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    if (s?.length == 0) et.error = "Field is required!"
                }

                override fun afterTextChanged(s: Editable?) {}
            })
            true
        }
        return MaterialAlertDialogBuilder(requireContext())
            .apply {
                setTitle("New Grinder")
                setView(binding.root)
                setNegativeButton("Cancel") {dialog, _ -> dialog.dismiss() }
                setPositiveButton("Add") {dialog, _ ->
                    if (manufacturerInput.text.toString() != "" && modelInput.text.toString() != "") {
                        val g = Grinder(null, manufacturerInput.text.toString(), modelInput.text.toString())
                        li.onGrinderAdded(g)
                        dialog.dismiss()
                    }
                }
            }.create()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    interface OnGrinderAddedListener {
        fun onGrinderAdded(g: Grinder)
    }
}