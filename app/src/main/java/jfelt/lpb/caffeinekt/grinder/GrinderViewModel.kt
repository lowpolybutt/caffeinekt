package jfelt.lpb.caffeinekt.grinder

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GrinderViewModel(app: Application) : AndroidViewModel(app) {

    private val grinderRepo: GrinderRepo

    val allGrinders: LiveData<List<Grinder>>

    init {
        val db = GrinderDatabase.getInstance(app).grinderDao()
        grinderRepo = GrinderRepo(db)
        allGrinders = db.getGrinders()
    }

    fun insertGrinder(g: Grinder) = viewModelScope.launch(Dispatchers.IO) { grinderRepo.insertGrinder(g) }
    suspend fun deleteGrinder(g: Grinder) = grinderRepo.deleteGrinder(g)
}