package jfelt.lpb.caffeinekt.grinder

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import jfelt.lpb.caffeinekt.MainActivity
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.databinding.BeanRecyclerItemBinding
import jfelt.lpb.caffeinekt.databinding.FragmentGrinderMainBinding

class GrinderFragmentMain : Fragment() {

    private var _binding: FragmentGrinderMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGrinderMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).supportActionBar?.title = "My Grinders"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv = binding.fragmentGrinderRv
        val llm = LinearLayoutManager(requireContext())
        val divider = DividerItemDecoration(requireContext(), llm.orientation)
        val adapter = GrinderRecycler(requireContext())
        rv.adapter = adapter
        rv.layoutManager = llm
        rv.addItemDecoration(divider)

        val model = ViewModelProvider(this)[GrinderViewModel::class.java]
        model.allGrinders.observe(viewLifecycleOwner, Observer {
            adapter.setGrinders(it)
        })
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private inner class GrinderRecycler(private val c: Context) : RecyclerView.Adapter<GrinderRecycler.GrinderHolder>() {

        private var grinders = emptyList<Grinder>()

        private inner class GrinderHolder(view: View) : RecyclerView.ViewHolder(view) {
            val manufacturer: TextView = view.findViewById(R.id.bean_item)
            val model: TextView = view.findViewById(R.id.bean_item_roaster)
        }

        override fun getItemCount() = grinders.size

        override fun onBindViewHolder(holder: GrinderHolder, position: Int) {
            holder.manufacturer.text = grinders[position].manufacturerName
            holder.model.text = grinders[position].modelName
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            GrinderHolder(BeanRecyclerItemBinding.inflate(LayoutInflater.from(c), parent, false).root)

        fun setGrinders(gs: List<Grinder>) {
            grinders = gs
        }
    }
}