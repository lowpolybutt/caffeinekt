package jfelt.lpb.caffeinekt.pourover

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import jfelt.lpb.caffeinekt.MainActivity
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.databinding.BeanRecyclerItemBinding
import jfelt.lpb.caffeinekt.databinding.FragmentPouroverMainBinding

class PouroverFragment : Fragment() {
    private var _binding: FragmentPouroverMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPouroverMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv = binding.fragmentPouroverRv
        val llm = LinearLayoutManager(requireContext())
        val decor = DividerItemDecoration(requireContext(), llm.orientation)
        val adapter = PouroverAdapter(requireContext())
        rv.adapter = adapter
        rv.layoutManager = llm
        rv.addItemDecoration(decor)

        val poVm = ViewModelProvider(this)[PouroverViewModel::class.java]
        poVm.allBrews.observe(viewLifecycleOwner, Observer {
            adapter.setBrewsList(it)
        })
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).supportActionBar?.title = "Pourover"
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private inner class PouroverAdapter internal constructor(private val c: Context) : RecyclerView.Adapter<PouroverAdapter.PouroverHolder>() {
        var brews = emptyList<Pourover>()

        private inner class PouroverHolder(view: View) : RecyclerView.ViewHolder(view) {
            val header: TextView = view.findViewById(R.id.bean_item)
            val subheader: TextView = view.findViewById(R.id.bean_item_roaster)
        }

        override fun getItemCount() = brews.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            PouroverHolder(BeanRecyclerItemBinding.inflate(LayoutInflater.from(c), parent, false).root)

        override fun onBindViewHolder(holder: PouroverHolder, position: Int) {
            holder.header.text = "${brews[position].bean.name} - ${brews[position].brewer}"
            holder.subheader.text ="${brews[position].coffeeMass}"
        }

        fun setBrewsList(pourovers: List<Pourover>) {
            brews = pourovers
            notifyDataSetChanged()
        }
    }
}