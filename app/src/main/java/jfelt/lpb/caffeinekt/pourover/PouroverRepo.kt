package jfelt.lpb.caffeinekt.pourover

import androidx.lifecycle.LiveData

class PouroverRepo(private val pouroverDao: PouroverDao) {

    val livePourovers: LiveData<List<Pourover>> = pouroverDao.getAllPourovers()

    suspend fun insertPourover(p: Pourover) = pouroverDao.insert(p)
    suspend fun deletePourover(p: Pourover) = pouroverDao.delete(p)
}