package jfelt.lpb.caffeinekt.pourover

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import jfelt.lpb.caffeinekt.MainActivity
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanViewModel
import jfelt.lpb.caffeinekt.bean.NewBeanDialog
import jfelt.lpb.caffeinekt.databinding.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class   PouroverActivity : AppCompatActivity(), NewBeanDialog.OnBeanAddedListener, OnPourAddedListener {

    private lateinit var binding: ActivityFilterNewBrewBinding

    private val beanVm by lazy { ViewModelProvider(this)[BeanViewModel::class.java] }

    private lateinit var pourAdapter: PourAdapter

    private lateinit var massBloomLayout: TextInputLayout
    private lateinit var massCoffeeLayout: TextInputLayout
    private lateinit var massWaterLayout: TextInputLayout

    private lateinit var selectedBean: Bean

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilterNewBrewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initBeanForm()
        initMassForm()

        val adapter = ArrayAdapter(this, R.layout.bean_dropdown_item, resources.getStringArray(R.array.pourover_brewers))
        binding.pouroverBrewDeviceSelector.adapter = adapter

        setSupportActionBar(binding.activityPouroverToolbar)
        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        binding.pouroverBreqPoursAddPour.setOnClickListener {
            val pd = NewPourDialog(this)
            pd.show(supportFragmentManager, pd.tag)
        }

        val rv = binding.pouroverBrewPoursList
        val llm = LinearLayoutManager(this)
        val decor = DividerItemDecoration(this, llm.orientation)
        pourAdapter = PourAdapter(this)
        rv.adapter = pourAdapter
        rv.layoutManager = llm
        rv.addItemDecoration(decor)

        binding.pouroverBrewContinue.setOnClickListener {
            storePourover()
        }
    }

    override fun onBeanAdded(b: Bean) {
        beanVm.addNewBean(b)
        // TODO this no worky
        binding.activityPouroverBrewBeanForm.aeropressBrewNewBean.isChecked = true
    }

    override fun onPourAdded(p: Pour) {
        pourAdapter.addPour(p)
        if (binding.pouroverBrewContinue.visibility == View.GONE) {
            binding.pouroverBrewContinue.visibility = View.VISIBLE
        }
    }

    private fun storePourover() {
        val vm = ViewModelProvider(this)[PouroverViewModel::class.java]
        val brew = Pourover(null, selectedBean, binding.pouroverBrewDeviceSelector.selectedItem.toString(),
        massBloomLayout.editText?.text?.toString()?.toInt(), massCoffeeLayout.editText?.text?.toString()?.toFloat()!!,
        massWaterLayout.editText?.text?.toString()?.toFloat()!!, pourAdapter.getAllPours())
        vm.viewModelScope.launch(Dispatchers.IO) {vm.insertPourover(brew)}
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun initBeanForm() {
        val useExistingBean = binding.activityPouroverBrewBeanForm.aeropressBrewNewBean
        val newBeanButton = binding.activityPouroverBrewBeanForm.aeropressBrewAddNewBeanButton
        val existingBeanPicker = binding.activityPouroverBrewBeanForm.aeropressChooseBeanLayout

        newBeanButton.setOnClickListener {
            val bd = NewBeanDialog(this, null)
            bd.show(supportFragmentManager, bd.tag)
        }

        useExistingBean.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                newBeanButton.visibility = View.GONE
                existingBeanPicker.visibility = View.VISIBLE
            } else {
                newBeanButton.visibility = View.VISIBLE
                existingBeanPicker.visibility = View.GONE
            }
        }

        val adapter = ArrayAdapter<Bean>(this, R.layout.bean_dropdown_item)
        (existingBeanPicker.editText as? AutoCompleteTextView)?.apply {
            setAdapter(adapter)
            setOnItemClickListener { _, _, position, _ ->
                selectedBean = adapter.getItem(position) !!
            }
        }

        beanVm.allBeans.observe(this, Observer {
            adapter.clear()
            adapter.notifyDataSetInvalidated()
            adapter.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun initMassForm() {
        val massForm = binding.filterBrewMasses
        massBloomLayout = massForm.aeropressBrewBloomTimeLayout
        massCoffeeLayout = massForm.aeropressBrewCoffeeMassLayout
        massWaterLayout = massForm.aeropressBrewWaterMassLayout
    }

    private inner class PourAdapter internal constructor(private val c: Context) : RecyclerView.Adapter<PourAdapter.PourHolder>() {
        private val inflater = LayoutInflater.from(c)
        private var pours = ArrayList<Pour>()

        private inner class PourHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val mass: TextView = itemView.findViewById(R.id.pourover_pour_mass)
            val time: TextView = itemView.findViewById(R.id.pourover_pour_time)
        }

        override fun getItemCount() = pours.size

        override fun onBindViewHolder(holder: PourHolder, position: Int) {
            holder.mass.text = "${pours[position].waterMass.toString()} g"
            holder.time.text = if (pours[position].duration == null) "N/A" else "${pours[position].duration?.toString()} s"
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            PourHolder(inflater.inflate(R.layout.pourover_pour_layout, parent, false))

        fun addPour(p: Pour) {
            pours.add(p)
            notifyDataSetChanged()
        }

        fun getAllPours(): List<Pour> = pours

        private fun removePour(i: Int) {
            pours.removeAt(i)
            notifyItemRemoved(i)
        }

    }
}

class NewPourDialog(private val li: OnPourAddedListener) : DialogFragment() {
    private var _binding: DialogNewPourBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = binding.root


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = DialogNewPourBinding.inflate(LayoutInflater.from(requireContext()))
        return MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle("New Pour")
            setView(binding.root)
            setNegativeButton("Cancel") {dialog, _ -> dialog.cancel() }
            setPositiveButton("Add") {dialog, _ ->
                val pourMass = binding.dialogPourWaterMassLayout.editText?.text?.toString()
                val pourTime = binding.dialogPourTimeLayout.editText?.text?.toString()
                val massStr = pourMass?.toFloat()!!
                val timeStr = pourTime?.toIntOrNull()
                val p = Pour(massStr, timeStr)
                li.onPourAdded(p)
                dialog.dismiss()
            }
        }.create()
    }
}

interface OnPourAddedListener {
    fun onPourAdded(p: Pour)
}