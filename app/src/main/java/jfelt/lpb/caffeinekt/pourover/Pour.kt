package jfelt.lpb.caffeinekt.pourover

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

data class Pour(val waterMass: Float, val duration: Int?) {
    override fun toString() = "Mass: $waterMass\nTime: $duration"
}

class PourConverter {
    @TypeConverter
    fun toJson(p: Pour): String {
        val t = object : TypeToken<Pour>(){}.type
        return Gson().toJson(p, t)
    }

    @TypeConverter
    fun toPour(j: String): Pour {
        val t = object : TypeToken<Pour>(){}.type
        return Gson().fromJson(j, t)
    }

    @TypeConverter
    fun toJson(p: List<Pour>): String {
        val t = object : TypeToken<List<Pour>>(){}.type
        return Gson().toJson(p, t)
    }

    @TypeConverter
    fun toPourList(j: String): List<Pour> {
        val t = object : TypeToken<List<Pour>>(){}.type
        return Gson().fromJson(j, t)
    }
}