package jfelt.lpb.caffeinekt.pourover

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class PouroverViewModel(app: Application) : AndroidViewModel(app) {

    private val pouroverRepo: PouroverRepo

    val allBrews: LiveData<List<Pourover>>

    init {
        val db = PouroverDatabase.getInstance(app).pouroverDao()
        pouroverRepo = PouroverRepo(db)

        allBrews = db.getAllPourovers()
    }

    suspend fun insertPourover(p: Pourover) = pouroverRepo.insertPourover(p)
    suspend fun deletePourover(p: Pourover) = pouroverRepo.deletePourover(p)
}