package jfelt.lpb.caffeinekt

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class Utils {
    companion object {
        const val DEFAULT_TAG = "CaffeineKt"
        fun debugLog(tag: String = DEFAULT_TAG, msg: String) = Log.d(tag, msg)
    }
}