package jfelt.lpb.caffeinekt.aeropress

import androidx.lifecycle.LiveData

class AeropressRepo(private val brewDao: AeropressDao) {

    val liveBrews: LiveData<List<Aeropress>> = brewDao.getAllAeropressBrews()

    suspend fun addNewBrew(a: Aeropress) = brewDao.insert(a)

    suspend fun deleteBrew(a: Aeropress) = brewDao.delete(a)
}