package jfelt.lpb.caffeinekt.aeropress

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanConverter

@Entity(tableName = "aeropress_brews")
data class Aeropress(@PrimaryKey(autoGenerate = true) val id: Int?, val bean: Bean, val bIsInverted: Boolean, val bloomTime: Int, val coffeeMass: Float, val waterMass: Float, val steepTime: Int, var rating: Int = 0)

@Dao
interface AeropressDao {
    @Query("SELECT * FROM aeropress_brews")
    fun getAllAeropressBrews(): LiveData<List<Aeropress>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(a: Aeropress)

    @Delete
    suspend fun delete(a: Aeropress)
}

@Database(entities = [Aeropress::class], version = 1, exportSchema = false)
@TypeConverters(BeanConverter::class)
abstract class AeropressBrewDatabase : RoomDatabase() {
    abstract fun aeropressDao(): AeropressDao

    companion object {
        @Volatile
        private var INSTANCE: AeropressBrewDatabase? = null

        fun getInstance(c: Context): AeropressBrewDatabase {
            val tmp = INSTANCE
            if (tmp != null) {
                return tmp
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    c.applicationContext,
                    AeropressBrewDatabase::class.java,
                    "aeropress_brew_db"
                ).apply {
                    fallbackToDestructiveMigration()
                }.build()
                INSTANCE = instance
                return instance
            }
        }
    }
}