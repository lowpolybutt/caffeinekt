package jfelt.lpb.caffeinekt.bean

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

@Entity(tableName = "beans")
data class Bean(@PrimaryKey(autoGenerate = true) val id: Int?, val name: String, val roaster: String, val roastProfile: String) {
    override fun toString(): String {
        return "$name, $roaster, $roastProfile"
    }
}

@Dao
interface BeanDao {
    @Query("SELECT * FROM beans")
    suspend fun getAllBeansStatic(): List<Bean>

    @Query("SELECT * FROM beans")
    fun getAllBeans(): LiveData<List<Bean>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(bean: Bean)

    @Delete
    suspend fun delete(bean: Bean)
}

@Database(entities = [Bean::class], version = 1, exportSchema = false)
abstract class BeanDatabase : RoomDatabase() {
    abstract fun beanDao(): BeanDao

    companion object {
        @Volatile
        private var INSTANCE: BeanDatabase? = null

        fun getDatabase(c: Context): BeanDatabase {
            val tmp = INSTANCE
            if (tmp != null) {
                return tmp
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    c.applicationContext,
                    BeanDatabase::class.java,
                    "bean_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

class BeanConverter {
    @TypeConverter
    fun fromBean(b: Bean): String? {
        val t = object : TypeToken<Bean>(){}.type
        return Gson().toJson(b, t)
    }

    @TypeConverter
    fun fromJson(j: String): Bean? {
        val t = object :  TypeToken<Bean>(){}.type
        return Gson().fromJson(j, t)
    }
}