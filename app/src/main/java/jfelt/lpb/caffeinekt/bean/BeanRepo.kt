package jfelt.lpb.caffeinekt.bean

import androidx.lifecycle.LiveData
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanDao

class BeanRepo(private val beanDao: BeanDao) {

    val liveBeans: LiveData<List<Bean>> = beanDao.getAllBeans()

    suspend fun getStaticBeans(): List<Bean> = beanDao.getAllBeansStatic()

    suspend fun addNewBean(bean: Bean) = beanDao.insert(bean)
    suspend fun removeBean(bean: Bean) = beanDao.delete(bean)
}