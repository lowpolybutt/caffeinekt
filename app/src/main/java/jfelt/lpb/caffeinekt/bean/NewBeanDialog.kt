package jfelt.lpb.caffeinekt.bean

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.databinding.DialogNewBeanBinding

class NewBeanDialog(private val li: OnBeanAddedListener, private val beanToEdit: Bean?) : DialogFragment() {

    private var _binding: DialogNewBeanBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = DialogNewBeanBinding.inflate(LayoutInflater.from(requireContext()), null, false)
        return MaterialAlertDialogBuilder(requireContext())
            .apply {
                setTitle("New Bean")
                setView(binding.root)
                setNegativeButton("Cancel") {dialog, _ -> dialog.cancel() }
                setPositiveButton("Add") {dialog, _ ->
                    val beanName = binding.beanNewName.text?.toString()!!
                    val beanRoaster = binding.beanNewRoaster.text?.toString()!!
                    val beanRoastLevel = binding.beanNewRoastLevel.text?.toString()!!
                    li.onBeanAdded(Bean(null, beanName, beanRoaster, beanRoastLevel))
                    dialog.dismiss()
                }
        }.create()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (beanToEdit != null) {
            binding.beanNewName.setText(beanToEdit.name)
            binding.beanNewRoaster.setText(beanToEdit.roaster)
            binding.beanNewRoastLevel.setText(beanToEdit.roastProfile)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnBeanAddedListener {
        fun onBeanAdded(b: Bean)
    }
}