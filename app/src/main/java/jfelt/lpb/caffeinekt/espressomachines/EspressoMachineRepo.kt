package jfelt.lpb.caffeinekt.espressomachines

class EspressoMachineRepo(private val dao: EspressoMachineDao) {
    val allMachines = dao.getAllMachines()

    suspend fun insertEspressoMachine(m: EspressoMachine) = dao.insert(m)
    suspend fun deleteEspressoMachine(m: EspressoMachine) = dao.delete(m)
}