package jfelt.lpb.caffeinekt.espressomachines

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EspressoMachineViewModel(app: Application) : AndroidViewModel(app) {
    private val repo: EspressoMachineRepo

    val allMachines: LiveData<List<EspressoMachine>>

    init {
        val db = EspressoMachineDatabase.getInstance(app).espressoMachineDao()
        repo = EspressoMachineRepo(db)
        allMachines = db.getAllMachines()
    }

    fun insertMachine(m: EspressoMachine) = viewModelScope.launch(Dispatchers.IO) { repo.insertEspressoMachine(m) }

    fun deleteMachine(m: EspressoMachine) = viewModelScope.launch(Dispatchers.IO) { repo.deleteEspressoMachine(m) }
}