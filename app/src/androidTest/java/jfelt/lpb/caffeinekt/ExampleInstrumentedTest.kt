package jfelt.lpb.caffeinekt

import android.content.Context
import android.content.pm.ApplicationInfo
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachine
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachineDao
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachineDatabase
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachineRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import java.io.IOException

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("jfelt.lpb.caffeinekt", appContext.packageName)
    }
}

@RunWith(AndroidJUnit4::class)
class RoomTests {
    private lateinit var dao: EspressoMachineDao
    private lateinit var db: EspressoMachineDatabase

    @Before
    fun createDb() {
        val c = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            c,
            EspressoMachineDatabase::class.java
        ).build()
        dao = db.espressoMachineDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() = db.close()

    @Test
    @Throws(Exception::class)
    fun testEspressoMachineReadAndWrite() {
        val m = EspressoMachine(null, "Test", "Test")
        MainScope().launch(Dispatchers.IO) {
            dao.insert(m)
        }
        val fetch = dao.getAllMachines()
        assert(fetch.value != null)
    }
}